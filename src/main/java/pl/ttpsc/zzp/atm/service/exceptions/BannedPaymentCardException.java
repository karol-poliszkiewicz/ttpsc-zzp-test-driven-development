package pl.ttpsc.zzp.atm.service.exceptions;

import pl.ttpsc.zzp.atm.model.PaymentCard;

public class BannedPaymentCardException extends Exception {

    public BannedPaymentCardException(PaymentCard paymentCard) {
        super(String.format("Payment card %s was banned: %s.", paymentCard.getCardNumber(), paymentCard.getBanned()));
    }
}
