package pl.ttpsc.zzp.atm.service;

import pl.ttpsc.zzp.atm.model.BankAccount;
import pl.ttpsc.zzp.atm.service.exceptions.LackFundsException;

import java.math.BigDecimal;

public interface BankAccountService {
   void withdraw(BankAccount account, BigDecimal value) throws LackFundsException;
}
