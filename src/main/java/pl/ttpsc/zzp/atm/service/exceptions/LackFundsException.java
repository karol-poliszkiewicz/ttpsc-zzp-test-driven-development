package pl.ttpsc.zzp.atm.service.exceptions;

import pl.ttpsc.zzp.atm.model.BankAccount;

public class LackFundsException extends Exception {

    public LackFundsException(BankAccount bankAccount) {
        super(String.format("Insufficient funds on account: %s.", bankAccount.getAccountNumber()));
    }
}
