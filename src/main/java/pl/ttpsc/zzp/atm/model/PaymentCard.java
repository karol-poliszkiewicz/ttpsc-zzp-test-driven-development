package pl.ttpsc.zzp.atm.model;

import java.sql.Timestamp;
import java.time.Instant;

public class PaymentCard {
    private String cardNumber;
    private String pin;
    private Timestamp banned;

    public PaymentCard(String cardNumber, String pin) {
        this.cardNumber = cardNumber;
        this.pin = pin;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getPin() {
        return pin;
    }

    public Timestamp getBanned() {
        return banned;
    }

    public void ban() {
        this.banned = Timestamp.from(Instant.now());
    }

    public boolean isBanned() {
        return banned != null;
    }
}
