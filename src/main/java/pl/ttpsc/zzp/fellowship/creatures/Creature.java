package pl.ttpsc.zzp.fellowship.creatures;

public interface Creature {
    String getName();
}
