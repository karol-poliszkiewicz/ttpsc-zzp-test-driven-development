package pl.ttpsc.zzp.buffer;

import org.assertj.core.api.WithAssertions;

/**
 * Zadanie 2
 *
 * Bufor cykliczny jest to struktura danych składająca się z tablicy o pewnym z góry ustalonym, maksymalnym rozmiarze oraz zmiennych przechowujących dwa indeksy
 * – startowy (głowa ang. head) oraz końcowy (ogon ang. tail).
 * Głowa wskazuje pierwszy element, który zostanie odczytany z bufora.
 * Akcja odczytu przesuwa indeks na kolejny element. Ogon zaś wskazuje na ostatni element jaki został zapisany.
 * Kolejna akcja zapisu przesuwa indeks na kolejny element.
 * Pierwszy element do odczytu nie musi znajdować się na początku bufora.
 *
 * !!! Zwróć uwagę na dwa zagadnienia: obsługa dojścia do tablicy oraz zjawisko przepełnienia.
 */
class CircularBufferTest implements WithAssertions {
    // TODO
}