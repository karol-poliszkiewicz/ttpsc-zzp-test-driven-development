package pl.ttpsc.zzp.fellowship;

import org.assertj.core.api.WithAssertions;

/**
 * Zadanie 1
 *
 * Przypomnienie testowania jednostkowego z assertJ wykorzystujac strukture Given-When-Then.
 *
 * Sprawdz:
 * 1. Czy w druzynie znajduje sie jakis czlowiek.
 * 2. Czy druzyna sklada sie z 9 czlonkow.
 * 3. Jakiej rasy jest czarodziej Gandalf.
 * 4. Czy w druzynie jest jakis Maiar
 * 5. Czy Balrog jest czlonkiem druzyny. - klasa Balrog
 * 6. Czy Boromir jest dokladnie pomiedzy Aragornem i Legolasem.
 * 7. Czy druzyna zawiera 4 hobbitow.
 * 8. Czy Saruman jest Valarem? - klasa Saruman
 */
class FellowshipTest implements WithAssertions {
    //TODO
}