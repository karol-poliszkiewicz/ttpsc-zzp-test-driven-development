package pl.ttpsc.zzp.atm;

import org.assertj.core.api.WithAssertions;

/**
 * Zadanie 3
 *
 * Zaimplementuj serwis bankomatu (klasa Atm), ktory powinien osblugiwac operacje:
 * 1. autoryzuj karte platnicza
 * 2. pobierz stan konta
 * 3. wyplac srodki
 *
 * Wykorzystaj framework mockito
 */
class AtmTest implements WithAssertions {
    // TODO
}